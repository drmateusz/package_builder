const { graphQLClient } = require('./apiClient.js');

module.exports = {

    
    // Returns a list of products
    async getProducts(auth, collection = "170460872801") {      
      let currentBatch;
      let data;
      let output = [];
      let cursor;
      let counter = 0;
      
        if(counter == 0){                    
          data = await graphQLClient(createQuery(1,"", collection), auth);          
          output = deconstructEdges(output, data['data']['data']['collection']['products']['edges']);
        }
        
        currentBatch = data['data']['data']['collection']['products'];    

        //check if there is another page
        while(currentBatch['pageInfo']['hasNextPage'] && counter <= 100){
          cursor = findLastCursor(currentBatch);          
          data = await graphQLClient(createQuery(1,cursor, collection), auth);                  
          output = deconstructEdges(output,data['data']['data']['collection']['products']['edges']);          
          currentBatch = data['data']['data']['collection']['products'];
          counter++;
        }    
        
        return(output);
    },
	
	 async getDiscounts(auth, collection = "custom_system_builder") {
		 let currentBatch;
		 let data;
		 let output = [];
		 let cursor;
		 let counter = 0;
		 
		 if(counter == 0){                    
			 data = await graphQLClient(createAutomaticDiscountQuery(), auth);         
			 //output = data['data']['data']['automaticDiscountNodes']['edges'][0]['node'];
		 }
		 
		 return auth;
	 }
}


function findLastCursor(data){  
  let lastCursor = data['edges'][data['edges'].length-1]['cursor'];
  return lastCursor;
}

function createQuery(limit = 15, cursor = "", collection = "170460872801"){
  
  if(cursor != ""){
    cursor = `, after: "${cursor}"`;
  }

  const collectionQuery = `{
    collection(id: "gid://shopify/Collection/${collection}") {
      products(first: ${limit}${cursor}) {
        pageInfo {
          hasNextPage
        }                         
        edges {
          cursor
          node {
              productType
              tags                                     
              title
              description
              descriptionHtml
              collections(first: 5) {
                edges {
                  node {
                    id
                    handle
                    title
                  }
                }
              }   
            variants(first: 5) {
              edges {
                node {
                  id
                  title
                  price
				  inventoryQuantity
                  availableForSale  
                }
              }
              edges{
                node{
                  image{
                    id
                    src
                  }
                }
              }
            }
            images(first: 10) {
              edges {
                node {
                  src
                  id
                }
              }
            }
          }
        }
      }    
    }
  }`;

  const query = `{
    products(first: ${limit}${cursor}) {
      pageInfo {
        hasNextPage
      }                         
      edges {
        cursor
        node {
            productType
            tags                                     
            title
            collections(first: 5) {
              edges {
                node {
                  id
                  handle
                  title
                }
              }
            }   
          variants(first: 5) {
            edges {
              node {
                id
                title 
				inventoryQuantity
                availableForSale
              }
            }
          }
          images(first: 1) {
            edges {
              node {
                src                                                                    
              }
            }
          }
        }
      }
    }
  }`;

  
  return collectionQuery;
}

function createAutomaticDiscountQuery(){
  const AUTOMATIC_QUERY = `{
    automaticDiscountNodes (first: 10) {
      edges {
        node {
          id
          automaticDiscount {
            __typename          
            ... on DiscountAutomaticBasic {            
              customerGets {              
                value {
                  __typename
                  ... on DiscountPercentage {
                    percentage
                  }               
                }
              }           
              title
            }
          }
        }
      }
     }
    }`;

    const QUERY = `{
      automaticDiscountNode(id: "gid://shopify/DiscountAutomaticNode/710124535905") {       
        automaticDiscount {
          __typename          
          ... on DiscountAutomaticBasic {            
            customerGets {              
              value {
                __typename
                ... on DiscountPercentage {
                  percentage
                }               
              }
            }           
            title
          }
        }        
      }
    }`

    return QUERY;
}

function deconstructEdges(output,data){  
  data.map((edge,index) => {
    output.push(edge);
  })

  return output
}