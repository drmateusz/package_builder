const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const SHOP_URL = 'https://ciekaweczytozrozumiesz-test2.myshopify.com';
module.exports = {
    findCollectionIDByHandle(handle, collections){        
        let collectionId;

        collections.map((collection, index) =>{
            if(collection['handle'] == handle){
                collectionId = collection['id'];
            }
        })

        return collectionId;
    },

    checkForDuplicates(package, currentPackages){         
        let found = null;        
            currentPackages.map((currentPackage,index)=>{
                if(package['token'] == currentPackage['token']){
                    found = index;
                }
            })
        return found;
    },

    prepareDataForCart(currentPackage){
        let items = [];
        let output = [];

        currentPackage['package'].map((item, index) => {
            items.push({
                id:item['id'],
                quantity:item['quantity'],
            })
        });

        output = {
            items:items
        }

        return output;
    },

    sendProductsToCart(data){        
        let systemRequest = new XMLHttpRequest(); systemRequest.open( "POST", 'https://ciekaweczytozrozumiesz-test2.myshopify.com/cart/add.js', false );                 
        systemRequest.setRequestHeader('Content-Type', 'application/json');    
        systemRequest.send( JSON.stringify({items: [{ id: '32694386819169', quantity: '2' }]}));
               
    }
}