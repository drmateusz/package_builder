const Router = require('koa-router');
const router = new Router();

const crypto = require('crypto')
const CURRENT_URL = "https:localhost:3000";
const { getProducts, getDiscounts } = require('./queries.js');
const aux = require('./auxFunctions.js');
//const { SHOPIFY_API_SECRET_KEY } = process.env;
const mysql = require('mysql');
const { createReadStream } = require ("fs");
const { unstable_renderSubtreeIntoContainer } = require('react-dom');
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const { default: createShopifyAuth } = require("@shopify/koa-shopify-auth");
const { SHOPIFY_API_SECRET_KEY, SHOPIFY_API_KEY } = process.env;


let currentProducts;

let con = mysql.createConnection({
    host: "localhost",
    user: "hybri_local",
    password: "Qg0yq%64",
    database: "hybrids1_local"
});

con.connect(function(err) {
    if (err) throw err;    
})

const prepareAuth = (ctx) => { 

    const accessToken = ctx.cookies.get("accessToken");
    const shopOrigin = ctx.cookies.get("shopOrigin");

    return {
        token: accessToken,
        shop: shopOrigin
	}
};


const getParams = function (url) {    
    const current_url = new URL(url);
    const search_params = current_url.searchParams;
    return search_params;	
};

// Create the 'products' route
router.get('/get_products/', async (ctx) => {
    ctx.set('Access-Control-Allow-Origin', '*');
    ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');    
    ctx.body = currentProducts;   
    return ctx;
});

router.get('/get_key/', async (ctx) => {	
	ctx.set('Access-Control-Allow-Origin', '*');
    ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');   
	
	
	
	createShopifyAuth({
        apiKey: SHOPIFY_API_KEY,
		//accessMode: "offline",
        secret: SHOPIFY_API_SECRET_KEY,
        scopes: ["read_products", "write_products", "read_price_rules", "write_price_rules", "read_discounts", "write_discounts"],
        afterAuth(ctx) {    
			
            console.log("> After authentication");
            const { shop, accessToken } = ctx.session;

            let sql = `DELETE FROM tokens WHERE shop = "${shop}"`;
            con.query(sql, function (err, result) {

                if (err) throw err;
                console.log("> Number of records deleted: " + result.affectedRows);

                sql = `INSERT INTO tokens(shop, token) VALUES ( "${shop}", "${accessToken}")`;
                con.query(sql, function (error, results, fields) {
                    if (error) throw error;
                    console.log('> Added one record to: ' + con.config.database);
                });
				

            });

            ctx.cookies.set("accessToken", accessToken, { httpOnly: false });
            ctx.cookies.set("shopOrigin", shop, { httpOnly: false });
            
            ctx.redirect("/");
        }
    })
	
	
	ctx.body = "HERE";
	return ctx;
})


router.post('/get_collection_products/', async (ctx) => {
    ctx.set('Access-Control-Allow-Origin', '*');
    ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');  
    
    //recieve handle from the builder    
    const handle = ctx.request.body['collection_handle'];

    //get token from db
   	const token = await getToken();
	
    let collections;
	let discounts;

  	await getCollections(ctx,token)
    .then((ctx) => {        
        collections = JSON.parse(JSON.parse(ctx.body));
    })
	
	
	


    // retrieving collection id of the sent from the builder
    const collection_ID = aux.findCollectionIDByHandle(handle, collections['custom_collections']);
    
    //prepare auth
    const auth = {
        token: token,
        shop: "ciekaweczytozrozumiesz-test2.myshopify.com"
    }		
    
    //get all products from the collection
    await getProducts(auth)
    .then((response) => {
        ctx.body = response
    });
       
})

 
// Create the 'products' route
router.get('/products/', async (ctx) => { 
    const auth = prepareAuth(ctx);    
	ctx.body = auth;
	return ctx
    await getProducts(auth)
    .then((response) => {
        ctx.body = response
    });
});

router.get('/builder/',  async (ctx) => {    
    const auth = prepareAuth(ctx); 
    
	let shopRe = /shop=([A-z-0-9.]+)/g;
	let shop = ctx["request"]["url"].match(shopRe);

	let prefixRe = /path_prefix=([A-z 0-9%\/]+)/g
	let prefix = ctx["request"]["url"].match(prefixRe);
	prefix = prefix[0].replace(/%2F/g,"/");

	let timeRe = /timestamp=([0-9]+)/g
	let time = ctx["request"]["url"].match(timeRe);

	let signatureRe = /signature=([A-z 0-9]+)/g
	let signature = ctx["request"]["url"].match(signatureRe);
	signature = signature[0].replace("signature=","");
	let string = prefix+shop+time;
    
    let digest = crypto.createHmac('SHA256', SHOPIFY_API_SECRET_KEY).update(string).digest("hex"); 		
 
    if(digest == signature){        
		
        await showResultsSequentially( auth, ctx );
    } else {
        console.log("> Authentication failed");
    }   
});
 
module.exports = {
    router
}

let updateAuth = (auth) => new Promise (( resolves, rejects )=>{
    //retrieve access token from the db and pass it into auth
    let sql = "SELECT *  FROM `tokens` WHERE `shop` LIKE 'ciekaweczytozrozumiesz-test2.myshopify.com'";    
    let result = con.query(sql, function(err, results, callback){
        if (err) throw err
        auth = results[0];  
        resolves(auth);      
    })    
})


let getToken = () => new Promise (( resolves, rejects )=>{
    //retrieve access token from the db and pass it into auth
	
    let output;
    let sql = "SELECT *  FROM `tokens` WHERE `shop` LIKE 'ciekaweczytozrozumiesz-test2.myshopify.com'";    
    let result = con.query(sql, function(err, results, callback){
        if (err) throw err        
        output = results[0];
         resolves(output['token']);		
    })    
})




let getCollections = (ctx, token) => new Promise (( resolves, rejects )=>{
    var getCollections = new XMLHttpRequest();
    getCollections.open( "GET", 'https://ciekaweczytozrozumiesz-test2.myshopify.com/admin/api/2020-07/custom_collections.json', false ); 
    getCollections.setRequestHeader('X-Shopify-Access-Token', token)
    getCollections.setRequestHeader('Content-Type', 'application/json');
    getCollections.send();    
    let response = getCollections.responseText;      
    ctx.body = JSON.stringify(response);    
    resolves(ctx);   
})


let showResultsSequentially = (auth, ctx) =>  Promise.resolve()
   // .then(()=> updateAuth(auth))    
    .then(() =>{        
        ctx.type = 'html';      
        ctx.body = createReadStream( __dirname + "/../public_proxy/index.html");        
        return ctx
    })   
    .catch((err) => console.error(err.message)) 
    











































    
    ///////////////////////////OLD CODE///////////////////////////


    //let currentPackages = [];
    // const parse = require('co-body');
    // const url = require('url');
    // const SHOP_URL = 'https://ciekaweczytozrozumiesz-test2.myshopify.com';
    // const axios = require('axios');
    
// //post package from builder
// router.post('/post_package/', async (ctx) => {
//     ctx.set('Access-Control-Allow-Origin', '*');
//     ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//     ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
//     ctx.body = ctx.request.body;
    
//     let currentPackage = [];
//     let packageCreatedTime = new Date().getTime();
//     let duplicate;
//     let packagedParsed = JSON.parse(ctx.request.body['package']);

//     //creating json with package
//     currentPackage = {
//         token       : ctx.request.body['token'],
//         package     : packagedParsed,
//         timeCreated : packageCreatedTime
//     }


//     //checking for duplicates
//     duplicate = aux.checkForDuplicates(currentPackage, currentPackages);
    
//     if(duplicate != null){
//         currentPackages.splice(duplicate,1);
//     }
    
//     currentPackages.push(currentPackage);
//     return ctx;
// })







// // Get all current systems
// router.get('/current_packages/', async (ctx) => { 
//     const auth = prepareAuth(ctx);
//     ctx.body = currentPackages;
//     return ctx;
// });


    
// router.get('/get_cart/', async (ctx) => {
//     ctx.set('Access-Control-Allow-Origin', '*');
//     ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//     ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');    
//     ctx.body = currentSystem;       
//     return ctx;
// });

// router.post('/post_system/', async (ctx) => {
//     ctx.set('Access-Control-Allow-Origin', '*');
//     ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//     ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');    
//     return ctx;
// });

// router.post('/new_post_current_cart/', async (ctx) => {
//     ctx.set('Access-Control-Allow-Origin', '*');
//     ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//     ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');    

//     let tokenToBeReturned;
//     let tokenExists = false;
//     let token = ctx.request.body['currentBundle'];
//     ctx.body = ctx.request.body;

//     //finding the current system on the server and returning it back to Shopify
//     currentSystem.map((system,index)=>{
//         if(system['token'] == token){
//             tokenExists = true;
//             tokenToBeReturned = index;
//         }
//     })

//     if(tokenExists){
//         ctx.body = currentSystem[tokenToBeReturned];
//     }

//     return ctx;
// });




// router.get('/add_discount/', async (ctx) => {
//     ctx.set('Access-Control-Allow-Origin', '*');
//     ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//     ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');

//     let output;
//     let token = await getToken();
//     await getDiscountCode(ctx,token)
//     .then((ctx) => {        
//         output = ctx.body;
//     })
        
//     let domain = "https://foo.bar.com";
//     let url = new URL (domain + ctx.request.url); 
//     let cartToken = url.searchParams.get('cart_token');
//     return output;
    
// });




// //endpoint adding new bundles to server
// router.post('/add_bundle_to_server/', async (ctx) => {
//     ctx.set('Access-Control-Allow-Origin', '*');
//     ctx.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//     ctx.set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');

//     let newBundle = ctx.request.body;

//     let newBundleParsed = {         
//         "token"     : newBundle['token'],
//         "amp"       : newBundle['amp'],
//         "speakers"  : newBundle['speakers'],
//         "cables"    : newBundle['cables'] 
//     }

//     let token = newBundle['token'];
//     let tokenExists = false;    
//     let tokenToBeRemoved;

//     currentSystem.map((system,index)=>{
//         if(system['token'] == token){
//             tokenExists = true;
//             tokenToBeRemoved = index;
//         }
//     })

//     //if the customer changes the bundle, it gets updated; if it's a new bundle it simply gets added to the list
//     if(tokenExists){
//         currentSystem[tokenToBeRemoved] = newBundleParsed;
//     } else {
//         currentSystem.push(newBundleParsed);
//     }    
//     return ctx;
// });


// let getDiscountCode = (ctx, token) => new Promise (( resolves, rejects )=>{
//     let code = new Date().getTime();

//     let data = {
//         "discount_code": {
//             "code": "custom_system_" + code
//         }
//     }

//     var createCode = new XMLHttpRequest();
//     createCode.open( "POST", 'https://ciekaweczytozrozumiesz-test2.myshopify.com/admin/api/2020-04/price_rules/684064047201/discount_codes.json', false ); 
//     createCode.setRequestHeader('X-Shopify-Access-Token', token)
//     createCode.setRequestHeader('Content-Type', 'application/json');
//     createCode.send( JSON.stringify(data) );    
//     let response = createCode.responseText;      
//     ctx.body = JSON.stringify(response);    
//     resolves(ctx);   
// })

// // Get all current systems
// router.get('/current_systems/', async (ctx) => { 
//     const auth = prepareAuth(ctx);
//     ctx.body = currentSystem;
//     return ctx;
// });


// // Delete current systems
// router.get('/delete_current_systems/', async (ctx) => { 
//     const auth = prepareAuth(ctx);    
//     currentSystem = [];
//     ctx.body = currentSystem;
//     return ctx;
// });
