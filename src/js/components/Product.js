import React, { Component } from "react";
import ReactDOM from "react-dom";
import  "./stylesheet.css"



class Product extends Component {
  constructor() {
    super();

    this.state = {
        open:false
    };

    this.expandProduct = this.expandProduct.bind(this);
    this.displayProduct = this.displayProduct.bind(this);
    this.displayOpenProduct = this.displayOpenProduct.bind(this);
  }


  componentDidMount(){
    
  }


  displayProduct(){
    let product = this.props.product['node'];
    let imageSrc = product['images']['edges'][0]['node']['src'];
    return(
        <div className="productContainer">
            <img src={imageSrc}></img>
        </div>
    ) 
  }

  displayOpenProduct(){
      
    let openProduct = document.querySelector(".openProduct");

    if(openProduct != null){
        openProduct.parentNode.removeChild(openProduct);
    }

    return(
        <div className="openProduct">
            {this.props.product['node']['title']}                                
        </div>
    )
    
  }

  expandProduct(){
    this.setState({
        open:!this.state.open
    })      
  }

    render() {
        return (
            <div className="product" cursor={this.props.product['cursor']} onClick={this.expandProduct}>
                {
                    this.props.product['node']['title'],
                    this.displayProduct()
                }
                {
                    (this.state.open ? (
                        this.displayOpenProduct()
                    ):(
                        null
                    ))
                }
            </div>
        );
    }
}

export default Product;

// const wrapper = document.getElementById("container");
// wrapper ? ReactDOM.render(<Product />, wrapper) : false;