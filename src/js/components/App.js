import React, { Component } from "react";
import ReactDOM from "react-dom";
import Summary from "./Summary.js"
import Builder from "./Builder.js"


const COLLECTION_HANDLE = "custom-system-builder";//""; //"summer-collection";//
const CURRENT_URL = "https://proofs.uvcreative.co.uk";

class App extends Component {
    constructor() {
        super();

        this.state = {
            fetchingCollection : true,            
            products: [],
            selectedProducts:[]
        };
    }

    async componentDidMount(){		
        await this.getCollectionProducts()
        .then((data)=>{
            this.setState({
                fetchingCollection : false,
                products: data
            })
        })
		
		
		
    }

    

    getCollectionHandleFromReferrer(){
        let referrer = document.referrer;
		
    }
	
	removeDisc(){		
		const disc = document.getElementById('toBeRemoved');
		disc.remove();
	}
	
    

    async getCollectionProducts(){        
        const body =  new URLSearchParams({
            'collection_handle': COLLECTION_HANDLE
        })
		
        let url = CURRENT_URL+"/get_collection_products/";
        
        // Default options are marked with *
        const response = await fetch(url, {
          method: 'POST', 
          mode: 'cors',
          cache: 'no-cache',
          credentials: 'same-origin', 
          headers: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          redirect: 'follow', 
          referrerPolicy: 'no-referrer',
          body: body.toString() 
        });
        return response.json(); 
    }

    render() {
        return (
            <div>
                {this.state.products.length == 0  ? (
                    <div>
				 {null}
                    </div>
                ):(
                    <div>
						{this.removeDisc()} 
						<Builder products={this.state.products}></Builder>
                    </div>                    
                )}                
            </div>
        );
    }

   
}

export default App;

// //if (typeof window !== 'undefined') {
//     React.render(<App />, document.getElementById("container"));
// //}

const wrapper = document.getElementById("container");
wrapper ? ReactDOM.render(<App />, wrapper) : false;







// async getCollections() {
    //     let url = CURRENT_URL + "/get_collections/";

    //     // Default options are marked with *
    //     const response = await fetch(url, {
    //       method: 'GET', 
    //       mode: 'cors',
    //       cache: 'no-cache',
    //       credentials: 'same-origin', 
    //       headers: {
    //         'Content-Type': 'application/json'            
    //       },
    //       redirect: 'follow', 
    //       referrerPolicy: 'no-referrer',
    //     });        

    //     return response.json();
    //   }


    

    // async getProductsFromCollection(){
    //     let collectionId = this.state.collection;

    //     const body =  new URLSearchParams({
    //         'collectionId': collectionId
    //     })

    //     let url = CURRENT_URL+"/get_products_from_collection/";

    //     // Default options are marked with *
    
    //     const response = await fetch(url, {
    //       method: 'POST', 
    //       mode: 'cors',
    //       cache: 'no-cache',
    //       credentials: 'same-origin', 
    //       headers: {
    //         "Content-Type": "application/x-www-form-urlencoded"            
    //       },
    //       redirect: 'follow', 
    //       referrerPolicy: 'no-referrer',
    //       body: body.toString() 
    //     });        

    //     return response.json();       
    // }


     // await this.getCollections()
        // .then( (data) => {            
        //     this.setState({
        //         collections:JSON.parse(data),
        //         fetchingCollection:false
        //     })                     
        // })

        // .then( () => {
        //     if(this.state.fetchingCollection == false){
        //         this.setState({
        //             collection:this.findCollectionIDByHandle(COLLECTION_HANDLE)
        //         })  
        //     }                       
        // })

        // .then( async () => {
        //     if(this.state.collection != null){
        //         await this.getProductsFromCollection()
        //         .then((data) => {
        //             this.setState({
        //                 products:data
        //             })
        //         })                
                
        //     }
        // })        