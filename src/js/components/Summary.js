import React, { Component } from "react";
import ReactDOM from "react-dom";


class Summary extends Component {
  constructor() {
    super();

    this.state = {
      value: ""
    };

    this.showSummary = this.showSummary.bind(this); 
  }


  componentDidMount(){
    this.showSummary();
  }

  showSummary(){
    let output = [];

    this.props.package.map((product, index)=>{   
      console.log(product);   
      output.push(
        <div>
          {product.id}
        </div>
      )
    })
  }


    render() {
      return (
        <div>          
            {this.props.package.length > 0 ? (
              this.showSummary()
            ):(
              console.log("false")
            )}        
        </div>
      );
    }
}

export default Summary;

// const wrapper = document.getElementById("container");
// wrapper ? ReactDOM.render(<Summary />, wrapper) : false;