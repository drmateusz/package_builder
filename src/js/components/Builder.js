import React, { Component } from "react";
import ReactDOM from "react-dom";
import Product from "./Product.js";
import Summary from "./Summary.js";
import "./stylesheet.css";

const STEPS = [
    ["amps", "non-amps"],
    ["speakers", "loud speakers"],
    ["cables", "wires"]
];

const STEPS_NAMES = [
    ["AMPS"],
    ["SPEAKERS"],
    ["CABLES"]
];

const STEPS_COPY = [
    ["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec dignissim mollis elementum. Praesent nec sapien et lectus imperdiet cursus in sit amet erat. Ut cursus blandit congue. Nam quis erat augue. Phasellus commodo, nisi fermentum vehicula maximus, turpis justo tempor turpis, id gravida massa dolor sed quam. Vivamus cursus risus id vestibulum vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit."],
    ["Aliquam suscipit sollicitudin augue id facilisis. Donec molestie turpis elit, eu facilisis sem congue non. Ut diam nisl, finibus et sapien at, vulputate venenatis metus. Donec id justo id dolor tincidunt efficitur tincidunt sed est. Donec a cursus enim. Integer semper ornare felis, at imperdiet sapien mollis at. Nulla lectus massa, ornare ac feugiat imperdiet, convallis ut lacus. Sed eget orci ullamcorper, fringilla est at, varius orci. In hac habitasse platea dictumst. Vestibulum eu porttitor metus. Aliquam auctor magna nulla, eu sollicitudin mi luctus et."],
    ["Vestibulum eu nulla elementum, imperdiet enim vel, consequat magna. Ut sed ligula euismod, feugiat mi id, volutpat turpis. Vivamus luctus nunc et tristique cursus. Suspendisse vel tristique quam. Proin varius auctor dui sed malesuada. Donec aliquam augue mauris, eu pellentesque magna condimentum vitae. Praesent laoreet pharetra augue non hendrerit. Quisque vel arcu risus. Etiam vehicula bibendum eros sagittis vehicula. Cras ultricies mollis imperdiet. Nulla sit amet viverra lorem."],
	["Donec mattis tristique ante, in tempus magna tincidunt eget. Vivamus id facilisis mauris. Suspendisse tincidunt magna mattis commodo vulputate. Donec ac aliquam metus, mollis lacinia nibh. Mauris aliquet gravida sapien, eget malesuada diam consectetur at. Sed cursus, enim nec convallis bibendum, nibh turpis auctor nibh, ut aliquet ante elit a massa. Nulla vel massa tempor, ornare sem vitae, posuere metus. Curabitur tempus at enim nec dictum. Donec id magna vel ligula interdum semper et non lacus. Nulla varius leo in dignissim sodales. Integer in laoreet magna. Vivamus massa odio, gravida et lacinia vel, consectetur id quam."]
];

const STEPS_DOUGBRADY = [
    //STEP 1
    [
        'Integrated Amplifier', 
        'Streaming Amplifier',
        'AV Amplifiers & Receivers',
        'Headphone Amplifier'
    ],
    //STEP 2
    [
        'Speakers - Floorstanding',
        'Loudspeakers -Standmount',
        'In-Ceiling Speaker',
        'In-Wall Speaker',
        'On Wall Speaker',
        'Outdoor Speakers',
        '5.1 Speaker Package',
        'Hi-Fi Headphones'
    ],
    //STEP 3
    [
        'Terminated Speaker Cable'
    ]
];
    
const CURRENT_URL = "https://821512b13cd2.ngrok.io";

class Builder extends Component {
    constructor() {
        super();
        this.state = {
            step : 0,            
            selectedProducts: [],
            selectedProduct: null,
            selectedQuantity: 0,
            mounted: false,
            element: null,
            products: [],
            promptVisible: false,
            discount: false
        };

        this.stepUp = this.stepUp.bind(this);
        this.expandProduct = this.expandProduct.bind(this);
        this.addToCart = this.addToCart.bind(this);   
        this.hidePrompt = this.hidePrompt.bind(this);         
    }

    componentDidMount(){    
        this.setState({
            element: document.querySelector(".builder")
        })

        this.addOpenToProducts();

        let cartRequest = new XMLHttpRequest();
        let cartUrl =  'https://ciekaweczytozrozumiesz-test2.myshopify.com/cart.js';
        cartRequest.open( "GET", cartUrl, false ); 
        cartRequest.send( null );  
		
		
    }   

    //add fields to products
    addOpenToProducts(){        
        let products = this.props.products;

        products.map((product,index) => {
            product['open'] = false;
            product['last_quantity'] = "1"  
            product['selectedVariant'] = this.getFirstVariantId(product);   
            product['mainImageInGallery'] = this.getFirstVariantImg(product);          
        })

        this.setState({
            products: products
        }, ()=> {
			console.log(this.state.products);
		})        
    }


    addToPackage(cursor){
        let products = this.state.products;
        let selectedProducts = this.state.selectedProducts;
        let productToBeAdded;
        let alreadyInPackage = false;
        let variantToBeRemoved;


        //loop through all products to find the one that is to be added to the package
        products.map((product,index) => {
            if(product['cursor'] == cursor){       

                productToBeAdded = {
                    "id"        :   product['selectedVariant'],
                    "quantity"  :   product['last_quantity'],                    
                    "cursor"    :   product['cursor'] //!!!!IF NEEDED, CURSOR CAN BE USED TO PREVENT ADDING MORE THAN ONE VARIANT OF THE SAME PRODUCT!!!!!
                }                

                //check if the selected variant already exists in the package         
                selectedProducts.map((selectedProduct, index ) => {                    
                    if(selectedProduct['id'] == productToBeAdded['id']){
                        
                        alreadyInPackage = true;
                        selectedProduct['quantity'] = productToBeAdded['quantity'];

                        if(selectedProduct['quantity'] < 1){
                            variantToBeRemoved = index;
                        }
                    }
                })

                //if the quantity of the variant is below 1, remove it from the package
                if(variantToBeRemoved!=null){
                    selectedProducts.splice(variantToBeRemoved,1);
                }
                
                //if it doesn't exist, add it to the package
                if(!alreadyInPackage){
                    selectedProducts.push(productToBeAdded)
                }                       
            }             
        })
        
        this.closeAllTabs();
        this.setState({
            selectedProducts:selectedProducts,
            promptVisible:true,
            selectedProduct:cursor,
        })
    }


    //EXPAND PRODUCT TAB AND CLOSE ANY OTHER THAT IS OPEN
    expandProduct(cursor){
        let products = this.state.products;
        let promptVisible = this.state.promptVisible
        if(promptVisible){
            return null;
        }

        products.map((product,index) => {
            if(product['cursor'] == cursor){
                product['open'] = true;
            } 
            
            else if (product['cursor'] != cursor && product['open'] == true){
                product['open'] = false;                                
            }
        })     
        
        this.setState({
            products:products
        })
    }

    //COLLAPSE PRODUCT TAB
    collapseProduct(cursor){
        let products = this.state.products;

        products.map((product,index) => {
            if(product['cursor'] == cursor){
                product['open'] = false;               
            }             
        })

        
        this.setState({
            products:products
        })
    }
    

    //making sure that there are only numbers in the quantity input
    sanitiseQuantityInput(value){
        const regex = /[^\d]+/g;        
        value = value.replace(regex, '');
        return value;
    }
	

	checkIfAvailable(cursor){
		let variants = this.getVariants(cursor);
		let output = false;
		
		variants.map((variant, index) =>{
			if(variant['node']['inventoryQuantity'] > 0){
				output = true;
			}			
		})
		
		return output;		
	}

    //filtering and showing products in the given steps (by productType)
    showProductsInStep(){
        let perLine = 3;
        let productsInLine = [];

        let step = this.state.step;
        let currentProductTypes = STEPS[step];
        let products = this.state.products;
        let component = [];
        let output = [];
        let currentProductType;     
		let cursor;
		let variants;
           
        products.forEach((product) => {                        
            currentProductType = product['node']['productType']; 			
			cursor = product['cursor'];
		
			
            if(currentProductTypes.includes(currentProductType) && this.checkIfAvailable(cursor) == true){
                component.push(product);
            }            
        })

        component.map(function (lineGroup, index) {                      
            let product = lineGroup['node'];
            let cursor = lineGroup['cursor'];
          
            let imageSrc = product['images']['edges'][0]['node']['src']
            if(lineGroup['open']){                
                productsInLine.push(     
                    <div className="productContainer" key={cursor} cursor={cursor} >
                        <img src={imageSrc} className="productContainerImage" onClick={()=> this.collapseProduct(cursor)}></img>
                        <div className="productContainerOpen">                            
                            <div className="productLeftBox">
                                <div className="productMainGalleryImage">
                                    {this.showMainImage(cursor)} 
                                </div>
                                <div className="productGallery">
                                    {this.showProductGallery(cursor)}                                                                                                             
                                </div>
                            </div>
                            <div className="productRightBox">
                                <div className="clickOut" onClick={()=> this.collapseProduct(cursor)}>
                                    <i className="fas fa-times"></i>
                                </div>
                                <div className="productTitle">
                                    {product['title']}
                                </div>
                                <div className="productPrice">
                                    £{this.getPrice(cursor)}
                                </div>
                                <div className="productVariants">                                    
                                    {this.displayVariants(cursor)}                                                                                                             
                                </div>
                                <div className="quantityAndSubmitBlock">
                                    <div className="productQuantity">
                                        <input type="text" defaultValue={lineGroup['last_quantity']} className="productQuantityInput" onChange={(e)=>this.updateLastQuantity(e, cursor)}></input>
                                    </div>
                                    <div className="addToPackage" onClick={()=> this.addToPackage(cursor)}>
                                        Add to Package
                                    </div> 
                                </div>

								


                                <div className="prproductDetails" dangerouslySetInnerHTML={{
                                    __html: product['descriptionHtml'].replace(/<\/p>/g, '<\/p><br>')
                                }}>                                                                                                                                               
                                </div>
                            </div>                                 
                        </div>                  
                    </div>                                              
                )
            } else {
                productsInLine.push(
                    <div className="productContainer" key={cursor} cursor={cursor} >
                        <img src={imageSrc} className="productContainerImage" onClick={()=> this.expandProduct(cursor)}></img>                                          
                        <div className="productInfoSmall">
                            <div className="productTitleSmall">
                                {product['title']}
                            </div>
                            
                            <div className="productPriceSmall">
                                £{this.getPrice(cursor)}
                            </div>
                        </div>
                    </div>   
                )
                // output.push (     
                //     <div className="productContainer" key={lineGroup['cursor']} cursor={lineGroup['cursor']} onClick={()=> this.expandProduct(lineGroup['cursor'])} >
                //         <img src={imageSrc}></img>                        
                //     </div>                                              
                //)
            } 
            
            
            if(((index+1) % 3) == 0){
                output.push(
                    <div className="productLine">
                        {productsInLine}
                    </div>
                );
                productsInLine = [];
            } else if (index == (component.length - 1)) {
                output.push(
                    <div className="productLine">
                        {productsInLine}
                    </div>
                );
            }
        }, this)

        return output;
        
    }

    //get variants from the given product by cursor
    getVariants(cursor){
        let products = this.state.products;
        let variants;

        products.map((product,index)=>{
            if(product['cursor'] == cursor){
                variants = product['node']['variants']['edges'];
            }
        })  

        return variants;
    }

    //get price of the selected variant of the given product
    getPrice(cursor){
        let products = this.state.products;
        let currentVariant;
        let variants = this.getVariants(cursor);
        let price;

        products.map((product,index)=>{
            if(product['cursor'] == cursor){
                currentVariant = product['selectedVariant'];
            }
        })

        variants.map((variant,index) => {
            if(variant['node']['id'] == `gid://shopify/ProductVariant/${currentVariant}`){
                price = variant['node']['price'];
            }
        })
        
        return price;
    }

    //finding images of the product 
    showProductGallery(cursor){
        let products = this.state.products;
        let selectedVariant;
        let images = [];
        let changableImage = [];
        let imagesAsHTML = [];
        let output = [];  
        let variants = this.getVariants(cursor);      
        let imageId;
		let perRow = 5;
		let imagesInRow = [];

        //retreiving images and current variant of the given product
        products.map((product,index)=>{
            if(product['cursor'] == cursor){
                images = product['node']['images']['edges'];
                selectedVariant = product['selectedVariant'];
            }
        });                
        
        //creating HTML img blocks for each image in the gallery
        images.map((image, index) => {
            imagesInRow.push(
                <div className={`productImage`} onClick={()=> this.updateMainImage(cursor, image['node']['src'])}>
                    <img src={image['node']['src']} className={'productSingleImage'}></img>
                </div>
            )
			
			
			if(((index+1) % perRow) == 0){
                imagesAsHTML.push(
                    <div className="imagesPerRow">
                        {imagesInRow}
                    </div>
                );
                imagesInRow = [];
            } else if (index == (images.length - 1)) {
                imagesAsHTML.push(
                    <div className="imagesPerRow">
                        {imagesInRow}
                    </div>
                );
            }
			
			
        })
		
		 
            
        
		 
            
       
       
        //pushing everything in to the output
        output.push(
            <div className={`productGalleryInnerContainer`}>
                {imagesAsHTML}
            </div>
        )
        return output;
    }

    //updating the main image of the given product
    updateMainImage(cursor, imageSrc){
        let products = this.state.products;
        products.map((product,index)=>{
            if(product['cursor'] == cursor){                
                product['mainImageInGallery'] = imageSrc;     
            }
        });

        this.setState({
            products:products
        })
    }

    //displaying the main image of the product (when the tab is open)
    showMainImage(cursor){
        let products = this.state.products;
        let mainImageSrc;
        let output = [];

        products.map((product,index) => {
            if(product['cursor'] == cursor){
                mainImageSrc = product['mainImageInGallery'];                    
            } 
        }) 

        output.push(
            <div className='mainImageContainer'>
                <img src={mainImageSrc} className="mainImageBox"></img>
            </div>
        );

        return output;
    }

    //display dropdown with other variants
    displayVariants(cursor){
        let variants = this.getVariants(cursor)
        let products = this.state.products;
        let defaultVariant;
        let variantId;
        let variantOptions = [];
        let output = []; 

        if(variants.length > 1){
            variants.map((variant,index) => {                
                variantId = this.extractId(variant['node']['id']);
                
                if(variant['node']['availableForSale'] > 0)
				{
					variantOptions.push(                    
                    	<option value={variantId} key={`key_${variantId}`}>{variant['node']['title']}</option>
                	)    
				}
                   
            })

            products.map((product,index) => {
                if(product['cursor'] == cursor){
                    defaultVariant = product['selectedVariant'];                    
                } 
            })            

            output.push(
                <div className="variant" key={`key1234`}>
                    <div className='variantText'>
                        Variants:
                    </div>
                   <select className="variantSelect" defaultValue={defaultVariant} onChange={(e) => this.updateSelectedVariant(e, cursor)}>
                        {variantOptions}
                    </select>
                </div>
            ) 
            return output;
        }        
    }

    //extracts the id from SHOPIFY's default id string
    extractId(id){
        const regex = /\d{1,}/g;
        const extractedId = id.match(regex);        
        return extractedId[0];         
    }

    //update selected variant on change in the dropdown
    updateSelectedVariant(e, cursor){
        let products = this.state.products;
        let currentVariant = e.target.value; 
        let variants = this.getVariants(cursor);
        let newMainImage;

        variants.map((variant,index) => {
            if(variant['node']['id'] == `gid://shopify/ProductVariant/${currentVariant}`){
                if(variant['node']['image'] != null){
                    newMainImage = variant['node']['image']['src']
                }
            }            
        });
        
        products.map((product,index) => {
            if(product['cursor'] == cursor){
                product['selectedVariant'] = currentVariant; 
                if(newMainImage != null){
                    product['mainImageInGallery'] = newMainImage; 
                }                   
            } 
        }) 

        this.setState({
            products:products
        })
    }

    //update quantity on change of input value 
    updateLastQuantity(e, cursor){        
        let products = this.state.products;
        let currentQuantity = e.target.value;        
       
        //sanitise the input value
        currentQuantity = this.sanitiseQuantityInput(currentQuantity);

        //update the input value
        document.querySelector(".productQuantityInput").value = currentQuantity;

        products.map((product,index) => {
            if(product['cursor'] == cursor){
                product['last_quantity'] = currentQuantity;                    
            } 
        });

        this.setState({
            products:products
        });        
    }

    //gets first variant's id, used only on page load
    getFirstVariantId(product){        
        return this.extractId(product['node']['variants']['edges'][0]['node']['id']);
    }

    //get the image of the first variant, if it doesn't exist get the first image of the product
    getFirstVariantImg(product){       
        if(product['node']['variants']['edges'][0]['node']['image'] != null){           
            return product['node']['variants']['edges'][0]['node']['image']['src'];
        } else {
            return  product['node']['images']['edges'][0]['node']['src'];
        }       
    } 

    //get the variant's image, if it doesn't exist get the first image of the product
    getVariantImgById(cursor, id){  

        let variants = this.getVariants(cursor);
        let product = this.getProduct(cursor)
        let output;
        variants.map((variant, index) => {
            if(variant['node']['id'] == `gid://shopify/ProductVariant/${id}`){
               
                if(variant['node']['image'] != null){                    
                    output = variant['node']['image']['src'];
                } else {                    
                    output = product['node']['images']['edges'][0]['node']['src'];
                }               
            }
        })
        
        return output;
    }

    //going to another step of the process
    stepUp(){

        this.closeAllTabs();

        this.setState({
            step:this.state.step + 1,
            promptVisible:false
        })
    }

    //closing all tabs that are open
    closeAllTabs(){
        
        let step = this.state.step;

        if(step > STEPS.length -1){
            return null;
        }

        let currentProductTypes = STEPS[step];
        let products = this.state.products;

        //closing any open tabs before getting to the next step
        products.forEach((product) => {
            if(currentProductTypes.includes(product['node']['productType'])){
                product['open'] = false;
            }
        })
    }

    //changing data format to be cart-readable
    prepareDataForCart(currentPackage){
        let items = [];
        let output = [];

        currentPackage.map((item, index) => {
            items.push({
                id:item['id'],
                quantity:item['quantity'],
                properties: {
                    'Note': "The discount is applicable only if the products are bought together."
                },   
            })
        });

        output = {
            items:items
        }

        return output;
    }

	showStepIntroduction(){
		let step = this.state.step;
		
		return(STEPS_COPY[step]);
	}

    //displaying steps at the top of the screen
    displaySteps(){
        let currentStep =  this.state.step;
        let output = [];
        let stepsCounter = 4;
        let stepDescription;

        for(let index = 1; index<=stepsCounter; index++){            
            if(index == 1){
                stepDescription = "Amps";
            } else if (index == 2) {
                stepDescription = "Speakers"
            } else if (index == 3) {
                stepDescription = "Cables"    
            } else if (index == 4) {
                stepDescription = "Summary"
            }  

            if(currentStep == index-1){
                output.push(
                    <div className="stepContainer">                    
                        <div className={`currentStep stepBox`} >
                            Step {index}
                        </div>
                        <div className='stepDescriptionBox'>
                            {stepDescription}
                        </div>
                    </div>
                )
            } else {
                output.push(
                    <div className="stepContainer">
                        <div className={` stepBox`} onClick={()=> this.changeStep(index-1)}>
                            Step {index} 
                        </div>
                        <div className='stepDescriptionBox'>
                            {stepDescription}
                        </div>
                    </div>
                )
            } 
        }

        return output;
    }

    roundUp(value, decimals) {
        return Number((Math.round(value + "e" + decimals)  + "e-" + decimals));
    }

    //changing step to the one that is being passed in
    changeStep(input){       
        this.closeAllTabs();
        this.setState({
            step: input
        })
    }

    //sending all products from the packfage to the cart
    async addToCart(){
        let selectedProducts = this.state.selectedProducts;
        let data = this.prepareDataForCart(selectedProducts);		
        //add products to cart
       
		let addProductsToCart = new XMLHttpRequest(); 
        addProductsToCart.open( "POST", 'https://ciekaweczytozrozumiesz-test2.myshopify.com/cart/add.js', false );                 
        addProductsToCart.setRequestHeader('Content-Type', 'application/json');    
        addProductsToCart.send( JSON.stringify(data) );         
        window.location.replace("https://ciekaweczytozrozumiesz-test2.myshopify.com/cart");
		
    } 
    
    
    removeFromPackage(cursor){;
        let productToBeRemoved;
        let selectedProducts = this.state.selectedProducts
        selectedProducts.map((product, index) => {
            if(product['cursor'] == cursor){
                productToBeRemoved = index;
            }
        })

        selectedProducts.splice(productToBeRemoved, 1);
        this.closeAllTabs();
        this.setState({
            selectedProducts:selectedProducts
        })
    }

    hidePrompt(){
        this.setState({
            promptVisible:false
        });
    }

    getProduct(cursor){
        let products = this.state.products;
        let output;
        products.map((product, index) => {
            if(cursor == product['cursor']){
                output = product;
            }
        })

        return output;
    }

    showPrompt(cursor){
        
        let products = this.state.products;
        let productTitle;
        let id;
        let variants = this.getVariants(cursor);

        products.map((product, index) => {
            if(cursor == product['cursor']){
                id = product['selectedVariant'];
                productTitle = product['node']['title'];
            }
        })

        variants.map((variant, index) => {
            if(variant['node']['id'] == `gid://shopify/ProductVariant/${id}`){
                if(variant['node']['title'] != "Default Title"){
                    productTitle = productTitle + " - " + variant['node']['title']
                }
                
            }
        })



        return(
        <div className='promptContainer'>
                            <div className='promptHeader'>
                                Product added to the Package!
                            </div>
                            <div className='promptCopy'>                                
                                You've added <span className='promptCopyBold'>{productTitle}</span> to the package. You can always check what products are in your package in the summary to the right.
                            </div>
                            <div className='promptCopy'>                                
                                Now you can move to the next step or carry on browsing this one.
                            </div>
                            <div className='promptBody'>
                                <div className='promptElement nextStep' onClick={this.stepUp}>
                                    <div className='promptText' >
                                        GO TO NEXT STEP
                                    </div>
                                </div>

                                <div className='promptElement browse' onClick={this.hidePrompt}>
                                    <div className='promptText'>
                                        CARRY ON BROWSING {STEPS_NAMES[this.state.step]}
                                    </div>    
                                </div>
                                
                            </div>    
                            
                        </div>  
        )  
    }

    showSummary(){

        let step = this.state.step;
        let output = [];
        let cursor;
        let variants;
        let imgSrc;
        let title;
        let productTitle;
        let id;
        let price;
        let totalPrice = 0;
        let quantity;
        let totalQuantity = 0;
        let selectedProducts = this.state.selectedProducts;
        let products = this.state.products;

        selectedProducts.map((selectedProduct, index)=>{

            id = selectedProduct['id'];              
            cursor = selectedProduct['cursor'];
            quantity = selectedProduct['quantity']
            variants = this.getVariants(cursor);

            totalQuantity = parseFloat(totalQuantity) + (parseFloat(quantity));

            //closing any open tabs before getting to the next step
            products.forEach((product) => {
                if(cursor == product['cursor']){
                    productTitle =  product['node']['title'];
                }
            })

            variants.map((variant,index)=>{
                if(variant['node']['id'] == `gid://shopify/ProductVariant/${id}`){
                    
                    price = variant['node']['price'];
                    totalPrice = parseFloat(totalPrice) + (parseFloat(price)*parseFloat(quantity));
                    totalPrice = this.roundUp(totalPrice, 2);
                    title = variant['node']['title'];
                    imgSrc = this.getVariantImgById(cursor,id)


                    if(title == 'Default Title'){
                        title = "";
                    } else {
                        title = " - "+title;
                    }

                    if(step <= STEPS.length - 1){
                        output.push(
                            <div className='summaryBox'>
                                <div className='summaryTitleAndPriceBox'>
                                    
                                    <div className='summaryTitle'>
                                        {productTitle}{title}
                                    </div>
    
                                    <div className='summaryQuantity'>
                                        x {quantity}
                                    </div>
                                    
                                    <div className='summaryPrice'>
                                        £{(price * quantity)}
                                    </div>
									<div className='summaryDelete' onClick={()=> this.removeFromPackage(cursor)}>
                                         <i className="far fa-trash-alt"></i>
                                    </div>
                                </div>                            
                            </div>
                        )
                    } else {
                        output.push(
                            <div className='summaryBox'>
                                <div className='summaryTitleAndPriceBox'>

                                   

                                    <div className='summaryImageTitleQunatityBox'>            
                                        <div className='summaryImage'>
                                            <img src={imgSrc}></img>
                                        </div>

                                        <div className='TitleQuantityBox'>
                                                <div className='summaryTitle'>
                                                    {productTitle}{title}
                                                </div>                                                
                                            
                                                <div className='summaryQuantity'>
                                                    x {quantity}
                                                </div>                                                
                                            
                                        </div> 
										   <div className='summaryPrice'>
                                        £{(price * quantity)}
                                    </div>

 									<div className='summaryDelete' onClick={()=> this.removeFromPackage(cursor)}>
                                         <i className="far fa-trash-alt"></i>
                                    </div>
                                    </div>                                    
                                    
                                 

                                </div>                            
                            </div>
                        ) 
                    }                    
                }
            })
        })

        //checking if the total number of items is 3 or greater, if yes, show total price with discount
        if(totalQuantity >= 3 ){

            output.push(
                <div className='totalPriceBox'>
                    <div className='totalNotDiscounted'>
                        <div className='totalText'>
                            Your saving:
                        </div>
                        <div className='totalPriceValue'>
                            £{this.roundUp(totalPrice - (parseFloat(totalPrice) * parseFloat(0.85)),2)}
                        </div>                            
                    </div>
                    <div className='totalDiscountedContainer'>
                        <div className='totalDiscountText'>
                            Total:
                        </div>
                        <div className='totalDiscountValue'>       
                            £{this.roundUp((parseFloat(totalPrice) * parseFloat(0.85)),2)}                
                           
                        </div>    
                    </div>                   
                </div>
            )
        } else {

            output.push(
                <div className='totalPriceBox'>
                    <div className='totalNotDiscounted'>
                        <div className='totalText'>
                            Total:
                        </div>
                        <div className='totalPriceValue'>
                            £{totalPrice}
                        </div>                            
                    </div>         
                    <div className='noDiscountDisclaimer'>
                        <div className='noDiscountDisclaimerText'>                       
                            Discount applies only if there are at least three products in the package.
                        </div>    
                    </div>           
                </div>
            )
        }
        
        
        return output;
    }

    render() {
        return (
            <div>     
                {this.state.step <= STEPS.length-1 ? (
                    <div className="builder">
                    <div className='builderIntro'>
                        Saving Package Builder
                    </div>
                    <div className="steps">
                        {this.displaySteps()}
                    </div>
                    <div className='stepIntro'>
						{this.showStepIntroduction()}
                    </div>
                   <div className='productsAndSummaryContainer'>                   
                        <div className="productsInStep">
                            {this.showProductsInStep()}                    
                        </div>
                        <div className='summaryContainer'>
                            <div className='summaryBoxTitle'>
                                Summary
                            </div>
                            {this.showSummary()}
                        </div>
                    </div>
                    
                    
                    {/* <div className="button" onClick={this.stepUp}>
                        NEXT STEP
                    </div> */}
                    <div className="addToCart" onClick={this.addToCart}>
                        ADD PACKAGE TO CART
                    </div>

                    {(this.state.promptVisible == true ? (
                        this.showPrompt(this.state.selectedProduct)                        
                    ):(
                        null
                    ))}          
                    
                </div>
                ):(
                    <div className="builder">
                    <div className='builderIntro'>
                        Saving Package Builder
                    </div>
                    <div className="steps">
                        {this.displaySteps()}
                    </div>
                    <div className='stepIntro'>
						{this.showStepIntroduction()}
                    </div>

                    <div className='summaryContainerLastStep'>
                        <div className='summaryBoxTitle'>
                            Summary
                        </div>
                        {this.showSummary()}
                    </div>

                    <div className='addToCartButton' onClick={this.addToCart}> 
                        SEND TO CART
                    </div>

                   
                    
                </div>
                )}          
                
            </div>
        );
    }
}

export default Builder;

// //if (typeof window !== 'undefined') {
//     React.render(<App />, document.getElementById("container"));
// //}

// const wrapper = document.getElementById("container");
// wrapper ? ReactDOM.render(<Builder />, wrapper) : false;