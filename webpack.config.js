const NodemonPlugin = require('nodemon-webpack-plugin'); // Ding
const path = require('path');

module.exports = {
    entry: './src/js/components/App.js',
    output: {
        path: path.resolve('./dist'),
        filename: 'main.js',
    },
    module: {
      rules: [
        {
          test: /\.css$/i,
          use: ['style-loader', 'css-loader'],
        },
        {
          test: /\.(js|jsx)$/,          
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        }
      ]
    },
    plugins: [
        new NodemonPlugin()
    ],
  };