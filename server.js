// Import Koa / Dotenv / Fetch modules
require("isomorphic-fetch");
const Koa = require("koa");
const koaStatic = require("koa-static");
const mount = require("koa-mount");
var bodyParser = require('koa-bodyparser');
const session = require("koa-session");
const cors = require('@koa/cors');
const dotenv = require("dotenv");
const mysql = require('mysql');
const _ = require('koa-route');
const fs = require('fs');
const nodemailer = require('nodemailer');


// Import Shopify/Koa modules to assist with authentication
const { default: createShopifyAuth } = require("@shopify/koa-shopify-auth");
const { verifyRequest } = require("@shopify/koa-shopify-auth");

// Env Configuration
dotenv.config();
const port = parseInt(process.env.PORT, 10) || 3000;
const { SHOPIFY_API_SECRET_KEY, SHOPIFY_API_KEY } = process.env;

// Create server using Koa
const server = new Koa();
server.use(session(server));

server.use(mount("/", koaStatic(__dirname + "/dist")));
server.keys = [SHOPIFY_API_SECRET_KEY];

// Import and use server-side routes
const { router } = require('./server/routes.js');

// Use module 'koa-bodyparser'
server.use(bodyParser({
    extendTypes: {
      json: ['application/x-javascript'] // will parse application/x-javascript type body as a JSON string
    }
  }));


server.use(router.routes());
server.use(router.allowedMethods());

// Authenticate app with Shopify
server.use(
    createShopifyAuth({
        apiKey: SHOPIFY_API_KEY,
		//accessMode: "offline",
        secret: SHOPIFY_API_SECRET_KEY,
        scopes: ["read_products", "write_products", "read_price_rules", "write_price_rules", "read_discounts", "write_discounts"],
        afterAuth(ctx) {    
			
            console.log("> After authentication");
            const { shop, accessToken } = ctx.session;

            let sql = `DELETE FROM tokens WHERE shop = "${shop}"`;
            con.query(sql, function (err, result) {

                if (err) throw err;
                console.log("> Number of records deleted: " + result.affectedRows);

                sql = `INSERT INTO tokens(shop, token) VALUES ( "${shop}", "${accessToken}")`;
                con.query(sql, function (error, results, fields) {
                    if (error) throw error;
                    console.log('> Added one record to: ' + con.config.database);
                });
				

            });

            ctx.cookies.set("accessToken", accessToken, { httpOnly: false });
            ctx.cookies.set("shopOrigin", shop, { httpOnly: false });
            
            ctx.redirect("/");
        }
    })
);




server.use(verifyRequest());

    





// Enable CORS (required to let Shopify access this API)
server.use(cors());

const dist = {    
    js : (ctx) => {     
        let js = fs.readFileSync('./dist/main.js', "UTF-8");
        ctx.body = js;
    }
}; 

server.use(_.get('/main.js', dist.js));


// server.use(_.get('/dist', () => {
//     return "test"
// }));

// Mount app on root path using compiled Vue app in the dist folder
server.use(mount("/", koaStatic(__dirname + "/public")));


// Start-up the server
server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
});

    let con = mysql.createConnection({
    host: "localhost",
    user: "hybri_local",
    password: "Qg0yq%64",
    database: "hybrids1_local"
}); 

    
    con.connect(function(err) {
        if (err) 
        console.log(err.message);        
      
    })

    


